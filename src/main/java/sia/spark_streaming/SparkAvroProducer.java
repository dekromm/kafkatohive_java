package sia.spark_streaming;

import com.twitter.bijection.Injection;
import com.twitter.bijection.avro.GenericAvroCodecs;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.Properties;

public class SparkAvroProducer {

    public static void main(String[] args) throws InterruptedException, IOException {
    	
    	Config config = ConfigFactory.load("C:\\Users\\jacopo.russo\\workspace\\spark-streaming_with_df\\src\\main\\resources\\application.conf");
    	Properties props = new Properties();

    	props.put("bootstrap.servers", "172.16.10.43:9092"); //props.put("bootstrap.servers", config.getString("kafka.bootstrap"));
    	props.put("client.id", "dns_group");
    	props.put("key.serializer", "org.apache.kafka.common.serialization.ByteArraySerializer");
    	props.put("value.serializer", "org.apache.kafka.common.serialization.ByteArraySerializer");
    	String kafkaTopic = "TestDnsTopic";
        KafkaProducer<String, byte[]> producer = new KafkaProducer<>(props);

        Schema.Parser parser = new Schema.Parser();

		ClassLoader classLoader =
				SparkConsumer.class.getClassLoader();


        Schema schema = null;
        //Schema schema = new Schema.Parser().parse(new File("C:\\Users\\jacopo.russo\\workspace\\spark-streaming_with_df\\src\\main\\avro\\DnsRecord.avsc"));
        URL res = Objects.requireNonNull(
				classLoader.getResource("DnsRecord.avsc"),
				"Can't find configuration file app.conf");
		try {
			schema = parser.parse(new File(res.getFile()));
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        Injection<GenericRecord, byte[]> recordInjection = GenericAvroCodecs.toBinary(schema);


        //for (int i = 0; i < 1000; i++) {
            GenericData.Record avroDns = new GenericData.Record(schema);
            avroDns.put("frame_time", "My_frame_time");
            avroDns.put("unix_tstamp", (long) 1494445644);
			avroDns.put("frame_len", 145);
			avroDns.put("ip_src", "My_src_ip");
			avroDns.put("ip_dst", "My_dest_ip");
			avroDns.put("dns_qry_name", "My_qry_name");		//controlla
			avroDns.put("dns_qry_class", "My_dns_qry_class");	
			avroDns.put("dns_qry_type", 1);
			avroDns.put("dns_qry_rcode", 1);	
			avroDns.put("dns_a", "My_dns_qry_a");

            byte[] bytes = recordInjection.apply(avroDns);

            ProducerRecord<String, byte[]> record = new ProducerRecord<>(kafkaTopic, bytes);
            producer.send(record);

          //  Thread.sleep(250);

        //}

        producer.close();
    }
}