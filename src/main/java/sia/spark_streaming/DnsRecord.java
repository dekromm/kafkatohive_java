package sia.spark_streaming;

import org.apache.avro.generic.GenericRecord;

import java.io.Serializable;

public class DnsRecord implements Serializable{

	private static final long serialVersionUID = 1L;
	String frame_time;
	Long unix_tstamp;
	Integer frame_len;
	String ip_dst;
	String ip_src;
	String dns_qry_name;
	String dns_qry_class;
	Integer dns_qry_type;
	Integer dns_qry_rcode;
	String dns_a;
	
	public DnsRecord(){}
	
	public DnsRecord(String csv){
		String[] csvArray = csv.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
		frame_time = csvArray[0];
		unix_tstamp = new Long(csvArray[1]);
		frame_len = new Integer(csvArray[2]);
		ip_dst = csvArray[3];
		ip_src = csvArray[4];
		dns_qry_name = csvArray[5];
		dns_qry_class = csvArray[6];
		dns_qry_type = new Integer(csvArray[7]);
		dns_qry_rcode = new Integer(csvArray[8]);
		dns_a = csvArray[9];
	}
	
	public DnsRecord(GenericRecord record){
		frame_time = record.get("frame_time")!=null ? record.get("frame_time").toString() : "\"\"";
		unix_tstamp = record.get("unix_tstamp")!=null ? (long) record.get("unix_tstamp") : 0;
		frame_len = record.get("frame_len")!=null ? (int) record.get("frame_len") : 0;
		ip_dst = record.get("ip_dst")!=null ? record.get("ip_dst").toString() : "\"\"";
		ip_src = record.get("ip_src")!=null ? record.get("ip_src").toString() : "\"\"";
		dns_qry_name = record.get("dns_qry_name")!=null ? record.get("dns_qry_name").toString() : "\"\"";
		dns_qry_class = record.get("dns_qry_class")!=null ? record.get("dns_qry_class").toString() : "\"\"";
		dns_qry_type = record.get("dns_qry_type")!=null ? (int) record.get("dns_qry_type") : 0;
		dns_qry_rcode = record.get("dns_qry_rcode")!=null ? (int) record.get("dns_qry_rcode") : 0;
		dns_a = record.get("dns_a")!=null ? record.get("dns_a").toString() : "\"\"";
	}

	public String testPrint(){
		return getFrame_time() + getUnix_tstamp() + getFrame_len() + getIp_dst() + getIp_src() + getDns_qry_name() + getDns_qry_class() + getDns_qry_type() + getDns_qry_rcode() + getDns_a();
	}
	
	public String getFrame_time() {
		return frame_time;
	}
	public void setFrame_time(String frame_time) {
		this.frame_time = frame_time;
	}
	public Long getUnix_tstamp() {
		return unix_tstamp;
	}
	public void setUnix_tstamp(Long unix_tstamp) {
		this.unix_tstamp = unix_tstamp;
	}
	public Integer getFrame_len() {
		return frame_len;
	}
	public void setFrame_len(Integer frame_len) {
		this.frame_len = frame_len;
	}
	public String getIp_dst() {
		return ip_dst;
	}
	public void setIp_dst(String ip_dst) {
		this.ip_dst = ip_dst;
	}
	public String getIp_src() {
		return ip_src;
	}
	public void setIp_src(String ip_src) {
		this.ip_src = ip_src;
	}
	public String getDns_qry_name() {
		return dns_qry_name;
	}
	public void setDns_qry_name(String dns_qry_name) {
		this.dns_qry_name = dns_qry_name;
	}
	public String getDns_qry_class() {
		return dns_qry_class;
	}
	public void setDns_qry_class(String dns_qry_class) {
		this.dns_qry_class = dns_qry_class;
	}
	public Integer getDns_qry_type() {
		return dns_qry_type;
	}
	public void setDns_qry_type(Integer dns_qry_type) {
		this.dns_qry_type = dns_qry_type;
	}
	public Integer getDns_qry_rcode() {
		return dns_qry_rcode;
	}
	public void setDns_qry_rcode(Integer dns_qry_rcode) {
		this.dns_qry_rcode = dns_qry_rcode;
	}
	public String getDns_a() {
		return dns_a;
	}
	public void setDns_a(String dns_a) {
		this.dns_a = dns_a;
	}
}
