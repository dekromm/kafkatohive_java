package sia.spark_streaming;

import com.twitter.bijection.Injection;
import com.twitter.bijection.avro.GenericAvroCodecs;
import kafka.serializer.DefaultDecoder;
import kafka.serializer.StringDecoder;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericRecord;
import org.apache.commons.io.IOUtils;
import org.apache.hadoop.util.Time;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaPairInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;
import scala.Tuple2;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class SparkConsumer {

    private static String appConfigFile = "./app.conf";

    private static String master;
    private static String zkQuorum;
    private static String group;
    private static String topicName;
    private static String numThreads;
    private static String kafkaAddr;
    private static String hdfsAddr;
    private static String hdfsPath;
    private static String hiveAddr;
    private static String dbName;
    private static String tableName;
    private static String recordType;

    private static String last_h = "not_initialized";
    private static String current_h = "";
    private static String hiveDriverName = "org.apache.hive.jdbc.HiveDriver";

    private static Schema schema = null;
    private static Injection<GenericRecord, byte[]> recordInjection;

    static {
        Schema.Parser parser = new Schema.Parser();

		/*ClassLoader classLoader =
                SparkConsumer.class.getClassLoader();

		// Make sure that the configuration file exists
		URL res = Objects.requireNonNull(
				classLoader.getResource("DnsRecord.avsc"),
				"Can't find schema DnsRecord.avsc");*/
        try {
            schema = parser.parse(new File("./DnsRecord.avsc"));
        } catch (IOException e) {
            System.out.println(new SimpleDateFormat("yy/MM/dd HH:mm:ss").format(Time.now()) + " [KAFKA2HDFS] ERR Can't find schema DnsRecord.avsc");
            System.exit(1);
        }
        recordInjection = GenericAvroCodecs.toBinary(schema);
    }

    public static void main(String[] args) throws InterruptedException {

        if (args.length == 1)
            appConfigFile = args[0];

        //Load configuration file
        readConfig();

        //Hive driver test
        try {
            Class.forName(hiveDriverName);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        }

        System.setProperty("hadoop.home.dir", System.getProperty("user.dir")); //needed to load winutils.exe and write parquet files
        System.setProperty("user.name", "hdfs");
        System.setProperty("HADOOP_USER_NAME", "hdfs");

        SparkConf conf = new SparkConf().setAppName("EnrichStreamingData").setMaster(master);
        conf.set("mapreduce.fileoutputcommitter.marksuccessfuljobs", "false");
        JavaStreamingContext jssc = new JavaStreamingContext(conf, new Duration(10000));
        HashMap<String, Integer> topicMap = new HashMap<>();
        topicMap.put(topicName, Integer.parseInt(numThreads));


        SparkSession spark = SparkSession
                .builder()
                .appName("Parquetter")
                .getOrCreate();

		/*
         * The following code creates the direct stream as described in https://spark.apache.org/docs/2.1.0/streaming-kafka-0-8-integration.html
		 */
        Set<String> topicSet = topicMap.keySet();
        Map<String, String> kafkaParams = new HashMap<>();
        kafkaParams.put("metadata.broker.list", kafkaAddr);
        kafkaParams.put("auto.offset.reset", "smallest");

        VoidFunction<JavaPairRDD<String, byte[]>> RDDHandlerFunctionAvro = new VoidFunction<JavaPairRDD<String, byte[]>>() {
            private static final long serialVersionUID = 1L;

            @Override
            public void call(JavaPairRDD<String, byte[]> rdd) throws Exception {
                System.out.println("--- New RDD with " + rdd.partitions().size()
                        + " partitions and " + rdd.count() + " records");

                if (rdd != null) {

                    /*---------- Works with light (non header prefixed) messages  -----------------------
				    Injection<GenericRecord, byte[]> recordInjection = GenericAvroCodecs.toBinary(schema);

				    /*---------- Works with header prefixed messages  -----------------------*/
                    Injection<GenericRecord, byte[]> recordInjection = GenericAvroCodecs.apply(schema);

                    List<Tuple2<String, byte[]>> result = rdd.collect();
                    if (result.size() > 0) {

                        Long processingTime = Time.now();
                        List<DnsRecord> kafkaMessages = new ArrayList<>();

                        for (Tuple2<String, byte[]> temp : result) {

                            GenericRecord record = recordInjection.invert(temp._2).get();
                            System.out.println(new SimpleDateFormat("yy/MM/dd HH:mm:ss").format(Time.now()) + " INFO [KAFKA2HDFS] got record: " + recordInjection.invert(temp._2).get());
                            kafkaMessages.add(new DnsRecord(record));
                            System.out.println("printed: " + record.toString());
                        }

                        Dataset<Row> df;
                        df = spark.createDataFrame(kafkaMessages, DnsRecord.class);

                        DateFormat dfor = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                        String str = dfor.format(Time.now());

                        String filename = "y=" + str.substring(6, 10) + "/m=" + str.substring(3, 5) + "/d=" + str.substring(0, 2) + "/h=" + str.substring(11, 13);
                        df.write().mode(SaveMode.Append).option("compression", "none").parquet("hdfs://" + hdfsAddr + hdfsPath + filename);

                        //Count partition-specific occurrences, if newly created force hive to recognize it
                        current_h = str.substring(11, 13);
                        if (!last_h.equals(current_h)) {
                            //query hive
                            Connection con = DriverManager.getConnection("jdbc:hive2://" + hiveAddr + "/" + dbName, "hive", "");
                            Statement stmt = con.createStatement();
                            stmt.execute("MSCK REPAIR TABLE " + tableName);
                            System.out.println(new SimpleDateFormat("yy/MM/dd HH:mm:ss").format(Time.now()) + " INFO [KAFKA2HDFS] Switching from partition h=" + last_h + " to h=" + current_h + ", poked hive to make it sense changes");

                            last_h = current_h;
                        }


                        processingTime = Time.now() - processingTime;
                        System.out.println(new SimpleDateFormat("yy/MM/dd HH:mm:ss").format(Time.now()) + " INFO [KAFKA2HDFS] File saved. Processing time: " + processingTime);
                    }
                }
            }
        };

        VoidFunction<JavaPairRDD<String, String>> RDDHandlerFunctioncsv = new VoidFunction<JavaPairRDD<String, String>>() {
            private static final long serialVersionUID = 1L;

            @Override
            public void call(JavaPairRDD<String, String> rdd) throws Exception {
                System.out.println("--- New RDD with " + rdd.partitions().size()
                        + " partitions and " + rdd.count() + " records");
                rdd.foreach(record -> System.out.println(new SimpleDateFormat("yy/MM/dd HH:mm:ss").format(Time.now()) + " INFO [KAFKA2HDFS] got record: " + record._2));

                if (rdd != null) {
                    List<Tuple2<String, String>> result = rdd.collect();
                    if (result.size() > 0) {

                        Long processingTime = Time.now();
                        List<DnsRecord> kafkaMessages = new ArrayList<>();

                        for (Tuple2<String, String> temp : result) {
                            kafkaMessages.add(new DnsRecord(temp._2));
                        }

                        Dataset<Row> df;
                        df = spark.createDataFrame(kafkaMessages, DnsRecord.class);

                        DateFormat dfor = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                        String str = dfor.format(Time.now());

                        String filename = "y=" + str.substring(6, 10) + "/m=" + str.substring(3, 5) + "/d=" + str.substring(0, 2) + "/h=" + str.substring(11, 13);
                        df.write().mode(SaveMode.Append).option("compression", "none").parquet("hdfs://" + hdfsAddr + hdfsPath + filename);

                        //Count partition-specific occurrences, if newly created force hive to recognize it
                        current_h = str.substring(11, 13);
                        if (!last_h.equals(current_h)) {
                            //query hive
                            Connection con = DriverManager.getConnection("jdbc:hive2://" + hiveAddr + "/" + dbName, "hive", "");
                            Statement stmt = con.createStatement();
                            stmt.execute("MSCK REPAIR TABLE " + tableName);
                            System.out.println(new SimpleDateFormat("yy/MM/dd HH:mm:ss").format(Time.now()) + " INFO [KAFKA2HDFS] Switching from partition h=" + last_h + " to h=" + current_h + ", poked hive to make it sense changes");

                            last_h = current_h;
                        }


                        processingTime = Time.now() - processingTime;
                        System.out.println(new SimpleDateFormat("yy/MM/dd HH:mm:ss").format(Time.now()) + " INFO [KAFKA2HDFS] File saved. Processing time: " + processingTime);
                    }
                }
            }
        };

        VoidFunction<JavaPairRDD<String, byte[]>> RDDHandlerFunctionTest = new VoidFunction<JavaPairRDD<String, byte[]>>() {
            private static final long serialVersionUID = 1L;

            @Override
            public void call(JavaPairRDD<String, byte[]> rdd) throws Exception {
                System.out.println("--- New RDD with " + rdd.partitions().size()
                        + " partitions and " + rdd.count() + " records");

                List<Tuple2<String, byte[]>> result = rdd.collect();
                for (Tuple2<String, byte[]> temp : result) {
                    Injection<GenericRecord, byte[]> recordInjection = GenericAvroCodecs.apply(schema);
                    GenericRecord record = recordInjection.invert(temp._2).get();
                    DnsRecord r = new DnsRecord(record);
                    System.out.println("printed: " + r.testPrint());
                }
            }
        };


        switch (recordType.toUpperCase()) {
            case "AVRO": {
                kafkaParams.put("key.deserializer", "org.apache.kafka.common.serialization.ByteArrayDeserializer");
                kafkaParams.put("value.deserializer", "org.apache.kafka.common.serialization.ByteArrayDeserializer");

                JavaPairInputDStream<String, byte[]> directKafkaStream = KafkaUtils.createDirectStream(jssc,
                        String.class, byte[].class, StringDecoder.class, DefaultDecoder.class, kafkaParams, topicSet);
                directKafkaStream.foreachRDD(RDDHandlerFunctionAvro);

                //--------------USE THIS FOR TESTING PURPOSE-------------------------
                //directKafkaStream.foreachRDD(RDDHandlerFunctionTest);

                break;
            }
            case "CSV": {
                kafkaParams.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
                kafkaParams.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

                JavaPairInputDStream<String, String> directKafkaStream = KafkaUtils.createDirectStream(jssc,
                        String.class, String.class, StringDecoder.class, StringDecoder.class, kafkaParams, topicSet);
                directKafkaStream.foreachRDD(RDDHandlerFunctioncsv);
                break;
            }
            default: {
                System.err.println(new SimpleDateFormat("yy/MM/dd HH:mm:ss").format(Time.now()) + " [KAFKA2HDFS] ERR incorrect recordType, only \"avro\" and \"csv\" are allowed");
                System.exit(1);
                break;
            }
        }


        jssc.start();
        jssc.awaitTermination();
    }

    private static void readConfig() {
        Properties prop = new Properties();

        try {
			/*ClassLoader classLoader =
					SparkConsumer.class.getClassLoader();

			// Make sure that the configuration file exists
			URL res = Objects.requireNonNull(
					classLoader.getResource(appConfigFile),
					"Can't find configuration file app.conf");*/

            if (validateProp(appConfigFile)) {
                InputStream is = new FileInputStream(appConfigFile);
                prop.load(is);
                if (prop.size() == 12) {
                    Set<String> arguments = new HashSet<>(Arrays.asList("master", "zkQuorum", "group", "topicName", "numThreads", "kafkaAddr", "hdfsAddr", "hdfsPath", "hiveAddr", "dbName", "tableName", "recordType"));
                    if (new HashSet<>(Collections.list(prop.keys())).equals(arguments)) {
                        master = prop.getProperty("master");
                        zkQuorum = prop.getProperty("zkQuorum");
                        group = prop.getProperty("group");
                        topicName = prop.getProperty("topicName");
                        numThreads = prop.getProperty("numThreads");
                        kafkaAddr = prop.getProperty("kafkaAddr");
                        hdfsAddr = prop.getProperty("hdfsAddr");
                        hdfsPath = prop.getProperty("hdfsPath");
                        hiveAddr = prop.getProperty("hiveAddr");
                        dbName = prop.getProperty("dbName");
                        tableName = prop.getProperty("tableName");
                        recordType = prop.getProperty("recordType");
                    } else {
                        System.err.println(new SimpleDateFormat("yy/MM/dd HH:mm:ss").format(Time.now()) + " [KAFKA2HDFS] ERR incorrect config parameters spelling! Should be: master, zkQuorum, group, topicName, numThreads, kafkaAddr, hdfsAddr, hdfsPath, hiveAddr, dbName, tableName");
                        System.exit(1);
                    }
                } else {
                    System.err.println(new SimpleDateFormat("yy/MM/dd HH:mm:ss").format(Time.now()) + " [KAFKA2HDFS] ERR incorrect number of config parameters!");
                    System.exit(1);
                }
            } else {
                System.err.println(new SimpleDateFormat("yy/MM/dd HH:mm:ss").format(Time.now()) + " [KAFKA2HDFS] ERR one or more lines doesn't follow the schema KEY=VALUE");
                System.exit(1);
            }
        } catch (IOException e) {
            System.out.println(new SimpleDateFormat("yy/MM/dd HH:mm:ss").format(Time.now()) + " [KAFKA2HDFS] ERR Can't find schema " + appConfigFile);
            System.exit(1);
        }
    }

    private static boolean validateProp(String res) throws IOException {
        String propString = IOUtils.toString(new FileInputStream(res), "UTF8");
        if (!propString.matches("^(\\w+\\s*=\\s*\\S+\\r?\\n)*(\\w+\\s*=\\s*\\S+)$")) {
            return false;
        }
        return true;
    }
}